package org.reactor.transport.websockets;

public enum  WebSocketResponseType {

    RESPONSE, BROADCAST
}
