package org.reactor.transport.speech.recognize;

public interface VoiceRecognizedListener {

    void voiceRecognized(String recognizedText);
}
